package com.semenov.springboot_postgresql_app.rest;

import com.semenov.springboot_postgresql_app.binding.Course;
import com.semenov.springboot_postgresql_app.service.CourseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Tag(name = "Course Controller",description = "Course API")
public class CourseRestController {
    @Autowired
    private CourseService courseService;

    @PostMapping("/course")
    @Operation(summary = "Create course")
    public ResponseEntity<String> createCourse(@RequestBody Course course) {
        String status = courseService.upsert(course);
        return new ResponseEntity<>(status, HttpStatus.CREATED);
    }

    @GetMapping("/course/{cid}")
    @Operation(summary = "Get course by Id")
    public ResponseEntity<Course> getCourse(@PathVariable Integer cid) {
        Course course = courseService.getById(cid);
        return new ResponseEntity<>(course, HttpStatus.OK);
    }

    @GetMapping("/courses")
    @Operation(summary = "Get all courses")
    public ResponseEntity<List<Course>> getAllCourses() {
        List<Course> allCourses = courseService.getAllCourses();
        return new ResponseEntity<>(allCourses, HttpStatus.OK);
    }

    @PutMapping("/course")
    @Operation(summary = "Update course")
    public ResponseEntity<String> updateCourse(@RequestBody Course course) {
        String status = courseService.upsert(course);
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @DeleteMapping("/course/{cid}")
    @Operation(summary = "Delete course")
    public ResponseEntity<String> deleteCourse(@PathVariable Integer cid) {
        String status = courseService.deleteById(cid);
        return new ResponseEntity<>(status,HttpStatus.OK);
    }
}
