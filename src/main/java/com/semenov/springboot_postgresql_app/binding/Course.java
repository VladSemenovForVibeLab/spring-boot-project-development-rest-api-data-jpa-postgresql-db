package com.semenov.springboot_postgresql_app.binding;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
@Table(name = "COURSE_DTLS")
@Schema(description = "Course entity")
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Schema(description = "course id",example = "1")
    private Integer cid;
    @Schema(description = "course name",example = "Java")
    private String name;
    @Schema(description = "course price",example = "9999")
    private Double price;
}
