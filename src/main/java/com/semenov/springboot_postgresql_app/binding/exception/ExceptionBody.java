package com.semenov.springboot_postgresql_app.binding.exception;


import lombok.*;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ExceptionBody {
    private String message;
    private Map<String,String> errors;

    public ExceptionBody(String message) {
        this.message = message;
    }

}

