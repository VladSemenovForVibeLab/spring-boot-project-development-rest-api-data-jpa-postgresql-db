package com.semenov.springboot_postgresql_app.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;

public class ApplicationConfig {
    @Bean
    public OpenAPI openAPI(){
        return new OpenAPI()
                .info(new Info()
                        .title("Course API")
                        .description("spring boot application for accounting for paid courses")
                        .version("0.0.1")
                        .contact(new Contact()
                                .name("Semenov Vladislav")
                                .email("ooovladislavchik@gmail.com")));

    }

}
