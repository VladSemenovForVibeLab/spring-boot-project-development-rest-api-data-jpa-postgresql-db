package com.semenov.springboot_postgresql_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootPostgresqlAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootPostgresqlAppApplication.class, args);
    }

}
